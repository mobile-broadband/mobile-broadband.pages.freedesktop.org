---
title: "Debugging"
linkTitle: "Debugging"
weight: 2
description: >
  Debugging issues with the ModemManager daemon
---

## Daemon debug logs

Gathering debug logs of both `NetworkManager` and `ModemManager` daemons involves several steps. Although these daemons allow to configure the log level manually while running, some of the issues to debug may require a full reboot of both processes, so the steps below try to cover the most generic case.

Note that the debug logs may contain sensitive information (e.g. PIN number, APN settings, SMS contents). Feel free to remove that information when providing logs to the developers, specifying which was the information removed.

The `NetworkManager` daemon debug logs would only be required to analyze connectivity related problems. If the issue is unrelated to data connectivity, just the `ModemManager` daemon debug log should be enough.

### When using systemd

Mask and stop the services:
```
$> sudo systemctl disable NetworkManager ModemManager
$> sudo systemctl stop NetworkManager ModemManager
```

Manually run with debug enabled:
```
$> sudo /usr/sbin/ModemManager --debug
$> sudo /usr/sbin/NetworkManager --debug --log-level=DEBUG
```

Now, reproduce your issue, and gather debug logs. Use Ctrl+C to stop both processes.

Then, unmask the services and start them:
```
$> sudo systemctl enable NetworkManager ModemManager
$> sudo systemctl start NetworkManager ModemManager
```

### When not using systemd

If you are not using systemd, you can avoid getting the processes automatically launched (e.g. by DBus activation) by moving the program files to another path:
```
$> sudo mv /usr/sbin/NetworkManager /
$> sudo mv /usr/sbin/ModemManager /
$> sudo killall -TERM NetworkManager
$> sudo killall -TERM ModemManager
```

Manually run with debug enabled (using the new path):
```
$> sudo /ModemManager --debug
$> sudo /NetworkManager --debug --log-level=DEBUG
```
Now, reproduce your issue, and gather debug logs. Use Ctrl+C to stop both processes.

Finally, recover the original places for the program files:
```
$> sudo mv /ModemManager /usr/sbin/
$> sudo mv /NetworkManager /usr/sbin/
```

## qmi-proxy or mbim-proxy debug logs

Sometimes debugging problems in ModemManager requires not only the daemon's own debug log, but also the verbose logs of the `qmi-proxy` or `mbim-proxy` processes, which are the ones synchronizing the access to the QMI or MBIM control ports. In order to gather these additional logs, explicitly kill the processes and manually launch them in a separate terminal BEFORE running `ModemManager --debug`:

```
$> sudo killall mbim-proxy
$> sudo /usr/libexec/mbim-proxy --no-exit --verbose

$> sudo killall qmi-proxy
$> sudo /usr/libexec/qmi-proxy --no-exit --verbose
```

Usually only one of these proxies is used, there is no need to debug both at the same time.
