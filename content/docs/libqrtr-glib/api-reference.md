---
title: "API reference"
linkTitle: "API reference"
weight: 1
description: >
  Reference manual for the libqrtr-glib library.
---

The `libqrtr-glib` API reference provides a detailed list of operations that may be performed with QRTR nodes.

## Online references

The most up to date API reference of the `libqrtr-glib` library is kept in the following location:

 * [Latest](https://www.freedesktop.org/software/libqmi/libqrtr-glib/latest/)

The full list of API references published is kept for future reference:

 * [1.2.2](https://www.freedesktop.org/software/libqmi/libqrtr-glib/1.2.2/)
 * [1.0.0](https://www.freedesktop.org/software/libqmi/libqrtr-glib/1.0.0/)

There is no API reference published for stable release updates that didn't have any API change.

## Local reference

The API reference is usually installed along with the `libqrtr-glib` library (sometimes as a separate distribution package) under `/usr/share/gtk-doc/html/libqrtr-glib/` and can be browsed locally via the [Devhelp](https://wiki.gnome.org/Apps/Devhelp) tool. The version of the installed reference will be the one applying to the `libqrtr-glib` library installed in the system.
