---
title: "libmbim"
linkTitle: "libmbim"
weight: 1
description: >
  The GLib-based libmbim library to use the MBIM protocol
---

![libmbim logo](/images/libmbim-logo.png)

This section provides information about the `libmbim` library.
