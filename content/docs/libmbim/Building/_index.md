---
title: "Building"
linkTitle: "Building"
weight: 3
description: >
  How to build and install the libmbim library.
---

This section provides information about how to build and install the `libmbim` library.
