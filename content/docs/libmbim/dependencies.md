---
title: "Dependencies"
linkTitle: "Dependencies"
weight: 2
description: >
  Build and runtime dependencies of the libmbim library.
---

## Common dependencies

Before you can compile the libmbim library, you will need at least the following tools:
 * A compliant C toolchain: e.g. `glibc` or `musl libc`, `gcc` or `clang/llvm`.
 * [pkg-config](https://www.freedesktop.org/wiki/Software/pkg-config/), a tool for tracking the compilation flags needed for libraries.
 * The [glib2](https://developer.gnome.org/glib/) library.
     * For libmbim >= 1.26, glib2 >= 2.56.
     * For libmbim >= 1.24, glib2 >= 2.48.
     * For libmbim >= 1.16 and < 1.24, glib2 >= 2.36
     * For libmbim >= 1.0  and < 1.16, glib2 >= 2.32

In addition to the previous mandatory requirements, the project also has several optional dependencies that would be needed when enabling additional project features:
 * [gtk-doc](https://developer.gnome.org/gtk-doc-manual/stable) tools, in order to regenerate the documentation.
 * [gobject-introspection](https://gi.readthedocs.io), in order to generate the introspection support.

## Dependencies when building libmbim 1.26 or later with meson

When building with meson, the following additional dependencies are required:
 * [meson](https://mesonbuild.com/).
 * [ninja](https://ninja-build.org/).

The following optional dependencies are available when building with meson:
 * [bash-completion](https://github.com/scop/bash-completion), in order to add completion support for the command line tools.

## Dependencies when building libmbim 1.26 or earlier with GNU autotools

When building with the GNU autotools, the following additional dependencies are required:
 * [make](https://www.gnu.org/software/make/)

There are two main ways to build the library using GNU autotools: from a git repository checkout and from a source release tarball. When building from a git checkout instead of from a source tarball, the following additional dependencies are required:
 * GNU autotools ([autoconf](https://www.gnu.org/software/autoconf/)/[automake](https://www.gnu.org/software/automake)/[libtool](https://www.gnu.org/software/libtool/)).
 * [Autoconf archive](https://www.gnu.org/software/autoconf-archive), in the 1.22 and 1.24 series exclusively.
